var gulp 			= require('gulp');
var less 			= require('gulp-less');
var minify_css 		= require('gulp-minify-css');
var jade 			= require('gulp-jade');
var uglify 			= require('gulp-uglify');
var jshint			= require('gulp-jshint');
var jshint_reporter = require('gulp-jshint-file-reporter');
var concat_js		= require('gulp-concat');
var imagemin		= require('gulp-imagemin');
var ftp 			= require('gulp-ftp');
var sftp 			= require('gulp-sftp');

// Retrieve Sprig Settings
var project 	= require('./gulp-settings.json');

// Deploy Tasks
var tasks;
if (project.settings.ftp.use === "yes") {
	tasks = ['jade', 'less', 'res', 'md', 'req', 'ftp'];
} else if (project.settings.sftp.use === "yes") {
	tasks = ['jade', 'less', 'res', 'md', 'req', 'sftp'];
} else {
	tasks = ['jade','less', 'res', 'md', 'req',];
}

// Compile Jade
gulp.task('jade', function() {
	console.log('Compiling Jade.');
	gulp.src('./app/jade/*.jade', {base:'./app/jade/'})
		.pipe(jade({"pretty":true}).on('error', function(err) {
			console.log('JADE ERR: '+err.message);
		}))
		.pipe(gulp.dest(project.settings.deploy_path));
});

// Compile Less and Minify
gulp.task('less', function() {
	console.log('Compiling and Minifying LESS.');
	gulp.src('./app/less/main.less')
		.pipe(less().on('error', function(err) {
			console.log('LESS ERR: '+err.message);
		}))
		.pipe(minify_css().on('error', function(err) {
			console.log('MINIFY ERR: '+err.message);
		}))
		.pipe(gulp.dest(project.settings.deploy_path+'/css'));
});

// Move Required Libs to Deploy
gulp.task('js', function() {
	console.log('JSHint, concatenating and minifying Javascript for deployment.');

	// Grab all required libs from bower_components
	js_src = project.bower_components.libs;
	for (var x = 0; x < js_src.length; x++) {
		js_src[x] = project.bower_components.base + js_src[x];
	}
	// Import all js in the js directory
	js_src.push('./app/js/**/*.js');

	console.log(js_src);

	// JSHint on custom JS code
	gulp.src('./app/js/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter(jshint_reporter, {
			filename: "./app/logs/jshint.log"
		}).on('error', function(err) {
			console.log('JSHINT LOG ERR: '+err);
		}));

	gulp.src(js_src)
		.pipe(concat_js("all.js").on('error', function(err) {
			console.log('REQ CONCAT ERR: '+err);
		}))
		.pipe(uglify().on('error', function(err) {
			console.log('JS UGLIFY ERR: '+err);
		}))
		.pipe(gulp.dest(project.settings.deploy_path+'/js'));
});

// Move components to Deploy
gulp.task('resources', function() {
	console.log('Moving resources for deployment.');
	gulp.src('./app/img/**/*', {base: './app/img'})
		.pipe(imagemin().on('error', function(err){
			console.log('IMAGE MIN ERR: '+err);
		}))
		.pipe(gulp.dest(project.settings.deploy_path+'/img'));
	gulp.src('./app/fonts/*')
		.pipe(gulp.dest(project.settings.deploy_path+'/fonts'));
	// If Bootstrap is enabled
	if (project.settings.bootstrap.use == "yes") {
		gulp.src('./app/bower_components/bootstrap/fonts/*')
			.pipe(gulp.dest(project.settings.deploy_path+'/fonts'))
	}
});

// Deploy via FTP
gulp.task('ftp', function() {
	if (project.settings.ftp.use === "yes") {
		console.log('Moving required libs for deployment.');
		return gulp.src(project.settings.deploy_path+'/**/*')
			.pipe(ftp({
            	host: project.settings.ftp.host,
            	user: project.settings.ftp.username,
            	pass: project.settings.ftp.password,
            	port: project.settings.ftp.port
        	}));
	} else {
		console.log("FTP not enabled. Please set 'use' to 'yes' in gulp-settings.json .");
	}
});

// Deploy via SFTP
gulp.task('sftp', function() {
	if (project.settings.sftp.use === "yes") {
		console.log('Moving required libs for deployment.');
		return gulp.src(project.settings.deploy_path+'/**/*')
			.pipe(sftp({
            	host: project.settings.sftp.host,
            	user: project.settings.sftp.username,
            	pass: project.settings.sftp.password,
            	port: project.settings.sftp.port
        	}).on('error', function(err) {
        		console.log("SFTP ERR: "+err);
        	}));
	} else {
		console.log("SFTP not enabled. Please set 'use' to 'yes' in gulp-settings.json .");
	}
});

// Default Gulp Tasks
gulp.task('default',['jade','less', 'js', 'resources']);

// Watch Files For Changes
gulp.task('watch', function() {
	console.log('Watching the app directory for changes...');
    gulp.watch('./app/views/**/*.jade', ['jade']);
    gulp.watch('./app/styles/**/*.less', ['less']);

});

